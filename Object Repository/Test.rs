<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Test</name>
   <tag></tag>
   <elementGuidId>e1d12434-0006-4095-9337-142ab17b87ec</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>285QuL75d8gdjg1eMkjvDZz1oHmaxF3wzcrqLZxPr0lckbSoW82q7B0qHPqBCL5HinE4tMnOzgVtN5hHawEP+MiJhxQ8IC/2S7zGDyfj6eZa1COivo1wD84FOMOX6ADIM4QAasehfy/G5ro7C0qkNl97vPvqwnkD9lklZveRdFU/MZOVHuz3tKvhrmzKnD8iVLCox0oZpumiXcbVLRKbA0PAMoc/CZdkyRXNaOZJJVZ5/hqGjwhsoDlDnEwe9qXuCYinZnQ6EzuPDfGnHnUQqmsveJXt/NsoFxIe2Blkkrun676mzUaz+ylC6GCjYRaBqyV41RlRWklBmVXbohU3c2/3gggo5degVNbp6YHuyUwNC18A5rFGN/4FOeOAyh9Pvr2Zr1Z1K3AoE9XHAoyGCcpM+GZhuXA2vjdvfuCP4lrAKeOP7hULj+J0vFlzSrOCuHoSVZdsED1lssCwnreirHL8rZ8ZmqAhWdEX0hdtJtZ/eofId0n7HJCo64NQDgLDGqa5gIPWJ00PymVZg/KV0PFIhtoyQuGaXzOAzOTBKEA9J/pntV4LWU+hLUZuFfiw8r+P14YAfEZuoCfeNyqfNyEZYEe4sjovIqNVMm6F/bzdbGPnFv63DsCXfcJyYgpQsNkbfmvXhwvwKLDYeHfG8wkSGQeVfBQ6QUd2WgeCT1Y=</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;birth_date\&quot;: \&quot;${birth_date}\&quot;,\n  \&quot;id_number\&quot;: \&quot;${id_number}\&quot;,\n  \&quot;legal_name\&quot;: \&quot;${legal_name}\&quot;,\n  \&quot;mobile_phone\&quot;: \&quot;${mobile_phone}\&quot;,\n  \&quot;surgate_mother_name\&quot;: \&quot;${surgate_mother_name}\&quot;\n}\n&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>ecc7c2b3-dcb1-4ada-9dca-ae9814c03d7a</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${token}</value>
      <webElementGuid>7a88cdff-a1d1-4387-bdda-f1983e3a0bad</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://dev-core-customer.kreditplus.com/api/v3/customer/validate-data</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
