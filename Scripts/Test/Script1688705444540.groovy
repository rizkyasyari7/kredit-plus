import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.text.SimpleDateFormat
import java.time.temporal.ChronoUnit

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String birth_date = '1980-01-01'
String id_number = '1234560101800000'
String legal_name = 'IQBAL NOL'
String mobile_phone = '08123456000'
String surgate_mother_name = 'BUNDA'

'Send Request to Url'
response = WS.sendRequestAndVerify(findTestObject('Object Repository/Test',
		[('${birth_date}') : birth_date, ('${id_number}') : id_number, ('${legal_name}') : legal_name, ('${mobile_phone}') : mobile_phone, ('${surgate_mother_name}') : surgate_mother_name]))

'JsonSlurper to Parse Response'
JsonSlurper slurper = new JsonSlurper()

'JsonSlurper to Parse Response'
Map parsedJson = slurper.parseText(response.getResponseBodyContent())

'Response from Url'
WS.comment(response.getResponseBodyContent())

'Get Code Response'
String code = parsedJson.code

'Get Message Response'
String message = parsedJson.message

'Get Data Response'
String data = parsedJson.data

'Get Errors Response'
String errors = parsedJson.errors

'Get Request_id Response'
String request_id = parsedJson.request_id

'Verify if Error Code is CORE-AUTH-004'
WS.verifyMatch(code, 'CORE-AUTH-004', false, FailureHandling.STOP_ON_FAILURE)